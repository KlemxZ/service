# Services 

Services API will expose endpoint and return the information about services based on response from the API in json format.

## Project Overview
Service API should accept three query parameters:
- timezone: (timezone)
- userId: (user id)
- cc: (country code)

Response should contain information about enabled services which are multiplayer, user-support and ads based on query parameters.

## Installation instructions
### Requirements
Project is based on Google app Engine java environment and his components. Code is written in Eclipse editor. Both need to be installed manualy.

####  Install Google Cloud SKD
1. [Download and install Java SE 8 Development Kit (JDK).](https://www.oracle.com/technetwork/java/javase/downloads/index.html "Donwload and install Java SE 8 Development Kit (JDK).")
2. [Download and install the latest version of the Google Cloud SDK.](https://cloud.google.com/sdk/docs/ "Download and install the latest version of the Google Cloud SDK.")

After both installations open Google Cloud SDK Shell and type two commands:
1. For installing the App Engine Java component:
`gcloud components install app-engine-java`

2. Authorize your user account:
`gcloud auth application-default login`

####  Install Eclipse IDE
1. [Download and extract Eclipse IDE for Enterprise Java Developers](https://www.eclipse.org/downloads/packages/ "Download and extract Eclipse IDE for Enterprise Java Developers").
2. After extracting open Eclipse and select Help > Eclipse Marketplace, search for Google Cloud and press install. Restart Eclipse when prompted.

#### Download json-simple library
In this project we will be using json-simple library for writing and reading in json format.
Go to [json-simple download site](https://code.google.com/archive/p/json-simple/downloads "json-simple download site") and download and extract file named [json-simple-1.1.1.jar](https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/json-simple/json-simple-1.1.1.jar "json-simple-1.1.1.jar")

## Creating new project
1. In Eclipse click the Google Cloud Platform toolbar button ![](https://cloud.google.com/eclipse/docs/images/toolbaricon.png), select Create New Project and click Google App Engine Standard Java Project. 
2. Enter a Project name.
3. Select the libraries App Engine, Google Cloud Endpoints and Objectify. 
4. Click Finish.

After creating new project you need to import the external json-simple library into the project.
- Click with the right mouse button on the project and select Properties. In a properties window select on the right side Java Build Path > Add External JARs > open the json-simple.jar file and click Apply.
- In the same properties window select on the left side Deployment Assembly > Add > Java Build Path Entries and select json-simple.jar.

## Project explanation

Within project create two java classes. One is for our main functions and one for storing data and API. 

#### StoreData
For storing the data we will be using [DataStoreService](https://cloud.google.com/appengine/docs/standard/java/javadoc/com/google/appengine/api/datastore/DatastoreService "DataStoreService")  with parameters timezone, userId and cc (country code). Data is stored using [entities](https://cloud.google.com/appengine/docs/standard/java/datastore/entities "entities") and [keys](https://cloud.google.com/appengine/docs/standard/python/datastore/keyclass "keys"). For example when you pass the query parameters (timezone, userId and cc), set and get funtions are retrieving or storing information based on userId and created key, so we can later get the entitiy properties simply based on key value.

DataStoreService:
```java
DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
```
Get function:
```java
try {    
	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	return (String) e.getProperty("userId");
}catch(Exception v){
	return v.toString();
}
```

Set funtion:
```java
String exsist = "";

try {    
	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	exsist = (String) e.getProperty("userId");
}catch(Exception v){
	exsist = null;
}

if(exsist != null && !exsist.isEmpty()){
	//System.out.println("UserId Found");
	getUserId();
	setApiClicks(apiClicks);
}
else{
	//System.out.println("UserId Not Found");
	user.setProperty("userId", userId);
	datastore.put(user);

	setApiClicks(apiClicks);
}
```

Create getData function where we will return information about enabled multiplayer, user-support and ads. 
- For the multiplayer we needed to count API clicks. We stored API clicks with DataStoreService into entity so we can later retrieve it using get functions. Multiplayer is enabled if the user is located in the US or if the user has used game more than 5 times.
- Customer support is enabled only on work days betwen 9:00 - 15:00 Ljubljana time.  For customer support we used [Calendar](https://docs.oracle.com/javase/7/docs/api/java/util/Calendar.html "Calendar"), [ZonedDateTime](https://docs.oracle.com/javase/8/docs/api/java/time/ZonedDateTime.html "ZonedDateTime") and [DateTimeFormatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html "DateTimeFormatter").
```java
Calendar startCal = Calendar.getInstance();  
startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){  
		    ZoneId z = ZoneId.of( "Europe/Ljubljana" ) ;
			ZonedDateTime zdt = ZonedDateTime.now( z ) ;
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH");
}
```
- Last, we checked external API for enabled ads based on response. If response is `{“ads”: “sure, why not!”}`, ads are enabled and if response is `{“ads”: “you shall not pass!”}` ads are disabled.

#### MainFunction
Contains GET call on the external API with query params for countryCode and basic authentication, as well as GET data from our API.
If the call on the external API is OK, we get response if the ads are enabled or disabled in json form.

```java
URL externalUrl = new URL("https://us-central1-o7tools.cloudfunctions.net/fun7-ad-partner" + "?" +queryParam +country);

HttpURLConnection  connection = (HttpURLConnection) externalUrl.openConnection();
String basicAuth = "Basic " + Base64.getEncoder().encodeToString((userName+":"+userPass).getBytes(StandardCharsets.UTF_8));

connection.setRequestMethod("GET");
connection.setRequestProperty ("Authorization", basicAuth);
```
After the call on external API we pass the response body from ads and make a call to our API for response information about enabled services in json format.
```java
StoreData s = new StoreData(timezone, user, country);
response.getWriter().println(s.getData(ads));
```
## Tests
To run an App Engine application locally we need to:
1. Select from tab Run > Run as > App Engine
2. Wait for server to configure and update our code.
3. Paste URL [http://localhost:8080/](http://localhost:8080/) into browser
4. Paste URL to see data stored with DataStore [http://localhost:8080/_ah/admin](http://localhost:8080/_ah/admin) into browser

After we paste [http://localhost:8080/](http://localhost:8080/)  URL into browser we get simple UI we made for easy sending query parameters for timezone, userId and cc (countryCode).
When we press the submit button we get response in json : 
```java
Connection ok
{"customerSupport":"enabled","ads":"disabled","multiplayer":"enabled"}
```

## Deploying to Google App Engine
Before deploying to the Google App Engine you need to create a project on [Cloud Console](https://console.cloud.google.com/projectselector2 "Cloud Console"). Set unique project name and click create button.
Wait project to create and then select it. All projects have a unique ID, in our case it was:
- `service-253216`

After selecting a project go to [App Engine](https://console.cloud.google.com/appengine "App Engine") tab and create Application for selected project. Choose region and then click Create app.

Inside Eclipse find file named `appengine-web.xml`.  Inside add code: 
```java
<application>Project_ID</application>
```
where "Project_ID" is in our case `service-253216`.
```java
<application>service-253216</application>
```
After setting appeengine-web.xml you need to right click on project and select Deploy to App Engine Standard. When deploying is finished we get the URL link: https://service-253216.appspot.com