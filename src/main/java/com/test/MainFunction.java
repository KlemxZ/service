package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


@WebServlet(
    name = "MainFunction",
    urlPatterns = {"/service/"}
)
public class MainFunction extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {

	  String userName = "fun7user";
	  String userPass = "fun7pass";
	  String queryParam = "countryCode=";
	  String ads = "";
	 
	  String timezone = request.getParameter("timezone");
	  String user = request.getParameter("userId");
	  String country = request.getParameter("cc");

	  String url = "https://us-central1-o7tools.cloudfunctions.net/fun7-ad-partner" + "?" +queryParam +country;
	  URL externalUrl = new URL(url.trim());
	  
	  HttpURLConnection  connection = (HttpURLConnection) externalUrl.openConnection();
	  String basicAuth = "Basic " + Base64.getEncoder().encodeToString((userName+":"+userPass).getBytes(StandardCharsets.UTF_8));
	  
	  connection.setRequestMethod("GET");
	  connection.setRequestProperty ("Authorization", basicAuth);
	  
	  try{
		  response.getWriter().println("Connection ok");
	  
		  BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	  
		  StringBuffer json = new StringBuffer();
		  String line;

		  while ((line = reader.readLine()) != null) {
		    json.append(line);
		  }
		  reader.close();
		  
		  String returnedResult = json.toString();
		  JSONParser parser = new JSONParser();
		  JSONObject jsonObject = (JSONObject) parser.parse(returnedResult);
		  
		  ads = jsonObject.get("ads").toString();
	  }
	  catch (Exception e){
		  e.printStackTrace();
	  }

	  
	 /* Gets me a: Error: Server Error?
	  if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){

	  }
	  else if(connection.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
		  response.getWriter().println("missing mandatory parameters");
	  }
	  else if(connection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
		  response.getWriter().println("invalid credentials");
	  }
	  else if(connection.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR){
		  response.getWriter().println("server error");
	  }
	  else{
		  System.out.println("GET request not worked");
	  }	 
	  */
	  
	  	StoreData s = new StoreData(timezone, user, country);
		response.getWriter().println(s.getData(ads));
  }
}