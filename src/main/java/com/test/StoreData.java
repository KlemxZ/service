package com.test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;



public class StoreData {
	
	private String timezone;
	private String userId;
	private String cc;
	
	private String multiplayer;
	private String userSupport;
	private String ads;
	
	private int apiClicks;
	
	Entity user;
	Key k;
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();	 
	
	public StoreData(String timezone, String userId, String cc) {
		this.timezone = timezone;
		this.userId = userId;
		this.cc = cc;
		k = KeyFactory.createKey("User", userId);
		user = new Entity("User", userId);		
	}
	
	public String getTimezone() {			

		try {    
	    	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	    	return (String) e.getProperty("timezone");
	    }catch(Exception v){
	    	return v.toString();
	    }

	}

	public void setTimezone(String timezone) {
		user.setProperty("timezone", timezone);
		datastore.put(user);
	}

	public String getUserId() {		
		try {    
	    	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	    	return (String) e.getProperty("userId");
	    }catch(Exception v){
	    	return v.toString();
	    }
	}

	public void setUserId(String userId) {		
		String exsist = "";
		
		try {    
	    	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	    	exsist = (String) e.getProperty("userId");
	    }catch(Exception v){
	    	exsist = null;
	    }

		if(exsist != null && !exsist.isEmpty()){
			getUserId();
			setApiClicks(apiClicks);
		}
		else{
			user.setProperty("userId", userId);
			datastore.put(user);
			
			setApiClicks(apiClicks);
		}
	}

	public String getCc() {
		try {    
	    	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	    	return (String) e.getProperty("cc");
	    }catch(Exception v){
	    	return v.toString();
	    }
	}

	public void setCc(String cc) {
		user.setProperty("cc", cc);
		datastore.put(user);
	}

	public int getApiClicks() {
		try {    
	    	Entity e = datastore.get(KeyFactory.createKey("User" , userId));
	    	return Integer.parseInt(e.getProperty("apiClick").toString());
	    }catch(Exception v){
	    	return 0;
	    }
	}

	public void setApiClicks(int apiClicks) {
		apiClicks = getApiClicks();
		apiClicks += 1;
		user.setProperty("apiClick", apiClicks);
		user.setProperty("userId", userId);
		datastore.put(user);
	}
		
	public void setData(){
		setUserId(userId);
	}
	
	public String getStatus(){
		JSONObject b = new JSONObject();
		b.put("userId", getUserId());
		b.put("apiClicks", getApiClicks());

		return b.toString();
	}
	
	public String getData(String ifAds){	
		setUserId(userId);
		
		Calendar startCal = Calendar.getInstance();   

		if(cc.equals("us")){
			multiplayer = "enabled";
		}
		else if(getApiClicks() >= 5){
			multiplayer = "enabled";
		}
		else{
			multiplayer = "disabled";
		}
		

		if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){  
		    ZoneId z = ZoneId.of( "Europe/Ljubljana" ) ;
			ZonedDateTime zdt = ZonedDateTime.now( z ) ;
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH");
	        String formattedString = zdt.format(formatter);
			
			int hours = Integer.parseInt(formattedString);
	
			if(hours >= 9 && hours <= 15){
				userSupport = "enabled";
			}
			else{
				userSupport = "disabled";
			}
		}
		else{ 
			userSupport ="disabled";
		}
		
		if(ifAds.equals("sure, why not!")){
			ads = "enabled";
		}
		else{
			ads = "disabled"; 
		}

		JSONObject b = new JSONObject();		
		b.put("multiplayer", multiplayer);
		b.put("customerSupport", userSupport);
		b.put("ads", ads);
		
		return b.toString();
	}
}
